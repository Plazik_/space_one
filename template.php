<?php
/**
 * @file
 * Process theme data.
 */

// Load theme functions.
include_once 'inc/theme.inc';

/**
 * Implements hook_html_head_alter().
 */
function space_one_html_head_alter(&$head_elements) {
  unset($head_elements['system_meta_content_type']);

  // Remove shortlink from <head> section.
  foreach ($head_elements as $key => $element) {
    if (isset($element['#attributes']['rel']) && $element['#attributes']['rel'] == 'shortlink') {
      unset($head_elements[$key]);
    }
  }
}

/**
 * Preprocess variables for the html template.
 */
function space_one_preprocess_html(&$vars) {
  // drupal_add_css('//fonts.googleapis.com/css?family=PT+Sans:400,700&subset=latin,cyrillic', array('type' => 'external'));

  $viewport = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'viewport',
      'content' => 'width=device-width, initial-scale=1.0',
    ),
  );
  drupal_add_html_head($viewport, 'viewport');
}

/**
 * Override or insert variables into the comment templates.
 */
function space_one_preprocess_comment(&$vars) {
  // Generate the timestamp, PHP "c" format is wrong.
  $vars['datetime'] = format_date($vars['comment']->created, 'short');
  $vars['submitted'] = format_string('<span class="comment-username">!username</span> <span class="comment-datetime">!datetime</span>',
    array(
      '!username' => $vars['author'],
      '!datetime' => $vars['datetime'],
    )
  );
}

/**
 * Change comment submit button and remove Home Page field.
 */
function space_one_form_comment_form_alter(&$form, &$form_state) {
  $form['author']['homepage']['#access'] = FALSE;
  $form['actions']['submit']['#value'] = t('Add question');
}

/**
 * Preprocess variables for block.tpl.php.
 */
function space_one_preprocess_block(&$vars) {
  $vars['title_attributes_array']['class'][] = 'block-title';
}

/**
 * Remove panels separator.
 */
function space_one_panels_default_style_render_region($vars) {
  $output = '';
  $output .= implode('', $vars['panes']);
  return $output;
}

/**
 * Override or insert variables into the node templates.
 */
function space_one_preprocess_node(&$vars) {
  // Datetime stamp formatted correctly to ISO8601.
  // PHP 'c' format is not proper ISO8601!
  $vars['datetime'] = format_date($vars['created'], 'custom', 'Y-m-d\TH:i:sO');
  // Publication date, formatted with time element.
  $vars['publication_date'] = '<time datetime="' . $vars['datetime'] . '" pubdate="pubdate">' . $vars['date'] . '</time>';

  // Last update variables.
  $vars['datetime_updated'] = format_date($vars['node']->changed, 'custom', 'Y-m-d\TH:i:sO');
  $vars['custom_date_and_time'] = date('jS F, Y - g:ia', $vars['node']->changed);

  // Last updated formatted in time element.
  $vars['last_update'] = '<time datetime="' . $vars['datetime_updated'] . '" pubdate="pubdate">' . $vars['custom_date_and_time'] . '</time>';

  // Remove from title hided text.
  if (($vars['view_mode'] == 'teaser') && isset($vars['content']['links']['node']['#links']['node-readmore']['title'])) {
    $vars['content']['links']['node']['#links']['node-readmore']['title'] = t('Read more');
  }

  // ARIA Role.
  $vars['attributes_array']['role'][] = 'article';
}

/**
 * Preprocess panels pane.
 */
function space_one_preprocess_panels_pane(&$vars) {
  $vars['title_heading'] = 'h3';
  $vars['classes_array'][] = 'block';
  $vars['title_attributes_array']['class'][] = 'block-title';
}

/**
 * Preprocess variables for the search block form.
 */
function space_one_preprocess_search_block_form(&$vars) {
  // Changes the search form to use the "search" input element attribute (HTML5)
  // We have to replace the string because FAPI don't know what type=search is,
  // i.e. no way we can do this in a form alter hook.
  $vars['search_form'] = str_replace('type="text"', 'type="search"', $vars['search_form']);
}

/**
 * Implements hook_views_pre_render().
 */
function space_one_views_pre_render(&$view) {
  if ($view->name == 'small_product_information') {
    if (!empty($view->result[0]->nid)) {
      $nid = $view->result[0]->nid;
      $view->query->pager->display->handler->options['link_url'] = url('node/' . $nid);
      $view->build_info['title'] = l($view->build_info['title'], 'node/' . $nid);
    }
  }

  if ($view->name == 'product_first_image') {
    if ($view->current_display == 'entity_view_1') {
      $file_id = variable_get('ebay_parsing_no_image_id');
      if (!empty($file_id) && isset($view->query->pager->display->handler->handlers['empty']['entity']->options['entity_id'])) {
        $view->query->pager->display->handler->handlers['empty']['entity']->options['entity_id'] = $file_id;
      }
    }
    if ($view->current_display == 'entity_view_2') {
      // При отсуствии картинки делаем заглушку ссылкой на ноду.
      if (!empty($view->args[0]) && is_numeric($view->args[0])) {
        $file_id = variable_get('ebay_parsing_no_image_id');
        $nid = $view->args[0];
        if (!empty($file_id)) {
          $file = entity_load('file', array($file_id));
          $file = reset($file);
          $entity = entity_view('file', array($file), 'file_thumbnail');
          $entity['file'][$file_id]['file']['#path']['path'] = 'node/' . $nid;
          $render = render($entity);

          if (isset($view->query->pager->display->handler->handlers['empty']['area_text_custom']->options['content'])) {
            $view->query->pager->display->handler->handlers['empty']['area_text_custom']->options['content'] = $render;
          }
        }
      }
    }
  }
}
