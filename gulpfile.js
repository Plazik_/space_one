var gulp = require('gulp'),
  sass = require('gulp-sass'),
  autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function () {
  gulp.src('scss/styles.scss')
    .pipe(sass())
    .pipe(autoprefixer({
      browsers: ['last 3 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('./css/'))
});

gulp.task('default', function () {
  gulp.start('sass');
  gulp.watch('scss/**', function () {
    setTimeout(function () {
      gulp.start('sass');
    }, 200);
  });
});
